<?php
return [
    'latest_projects' => 'Последние работы',
    'from_blog' => 'Последние записи блога',
    'comments' => 'комментарий|комментария|комментариев',
    'read_more' => ' подробнее',
    'articles_no' => 'Записей нет!',
    'latest_project' => 'Последние работы',
    'latest_comments' => 'Последние комментарии',
    'replay' => 'Ответить'
];